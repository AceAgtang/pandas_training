import pandas as pd
import plotly.express as px
import plotly.io as pio
import matplotlib.pyplot as plt

def get_countries_with_confirmed_cases_and_deaths(data):
    # list countries with all cases of Novel Coronavirus (COVID-19) died.
    covid_data = pd.read_csv()
    data = covid_data.groupby('Country/Region')['Confirmed', 'Deaths', 'Recovered'].sum().reset_index()
    result = data[data['Confirmed'] == data['Deaths']]
    result = result[['Country/Region', 'Confirmed', 'Deaths']]
    result = result.sort_values('Confirmed', ascending=False)
    result = result[result['Confirmed'] > 0]
    result = result.reset_index(drop=True)
    print(result)

def get_top_ten_data_by_country(data):
    # top 10 countries data (Last Update, Country/Region, Confirmed, Deaths, Recovered) of Novel Coronavirus (COVID-19).
    covid_data = pd.read_csv(data,
        usecols=['Last Update', 'Country/Region', 'Confirmed', 'Deaths', 'Recovered'])
    result = covid_data.groupby('Country/Region').max().sort_values(by='Confirmed', ascending=False)[:10]
    pd.set_option('display.max_column', None)
    print(result)

def show_world_wide_cases_overtime(data, template):
    # Worldwide Confirmed Novel Coronavirus (COVID-19) cases over time
    pio.templates.default = template
    covid_data = pd.read_csv(data)
    grouped = covid_data.groupby('Last Update')['Last Update', 'Confirmed', 'Deaths'].sum().reset_index()
    fig = px.line(grouped, x="Last Update", y="Confirmed",
                  title="Worldwide Confirmed Novel Coronavirus(COVID-19) Cases Over Time")
    fig.show()

def show_confirmed_active_recovered_deaths_cases_by_states_us_bar(data):
    # state/province wise combine number of confirmed, deaths, recovered, active Novel Coronavirus cases in USA
    covid_data = pd.read_csv(data)
    covid_data['Active'] = covid_data['Confirmed'] - covid_data['Deaths'] - covid_data['Recovered']
    combine_us_data = covid_data[covid_data['Country/Region'] == 'US'].drop(['Country/Region', 'Latitude', 'Longitude'],
                                                                            axis=1)
    combine_us_data = combine_us_data[combine_us_data.sum(axis=1) > 0]
    combine_us_data = combine_us_data.groupby(['Province/State'])[
        'Confirmed', 'Deaths', 'Recovered', 'Active'].sum().reset_index()
    combine_us_data = pd.melt(combine_us_data, id_vars='Province/State',
                              value_vars=['Confirmed', 'Deaths', 'Recovered', 'Active'], value_name='Count',
                              var_name='Case')
    fig = px.bar(combine_us_data, x='Province/State', y='Count', text='Count', barmode='group', color='Case',
                 title='USA State wise combine number of confirmed, deaths, recovered, active COVID-19 cases')
    fig.show()

def show_deaths_cases_by_states_us_bar(data):
    # state/province wise combine number of confirmed, deaths, recovered, active Novel Coronavirus (COVID-19) cases in USA
    covid_data = pd.read_csv(data)
    us_data = covid_data[covid_data['Country/Region'] == 'US'].drop(['Country/Region', 'Latitude', 'Longitude'], axis=1)
    us_data = us_data[us_data.sum(axis=1) > 0]
    us_data = us_data.groupby(['Province/State'])['Deaths'].sum().reset_index()
    us_data_death = us_data[us_data['Deaths'] > 0]
    state_fig = px.line(us_data_death, x='Province/State', y='Deaths',
                       title='State wise deaths reported of COVID-19 in USA', text='Deaths')
    state_fig.show()
def show_deaths_cases_by_states_us_line(data):
    return
    # state/province wise combine number of confirmed, deaths, recovered, active Novel Coronavirus (COVID-19) cases in USA


def show_confirmed_cases_by_states_us(data):
    return
    # state/province wise Active cases of Novel Coronavirus (COVID-19) in USA

def show_death_cases_by_states_us(data):
    covid_data = pd.read_csv(data)
    us_data = covid_data[covid_data['Country/Region'] == 'US'].drop(['Country/Region', 'Latitude', 'Longitude'], axis=1)
    us_data = us_data[us_data.sum(axis=1) > 0]
    us_data = us_data.groupby(['Province/State'])['Deaths'].sum().reset_index()
    us_data_death = us_data[us_data['Deaths'] > 0]
    state_fig = px.bar(us_data_death, x='Province/State', y='Deaths',
                       title='State wise deaths reported of COVID-19 in USA', text='Deaths')
    state_fig.show()

    # state/province wise death cases of Novel Coronavirus (COVID-19) in USA

def show_death_cases_by_province_china(data):
    # state/province wise death cases of Novel Coronavirus (COVID-19) in China
    covid_data = pd.read_csv(data)
    us_data = covid_data[covid_data['Country/Region'] == 'China'].drop(['Country/Region', 'Latitude', 'Longitude'], axis=1)
    us_data = us_data[us_data.sum(axis=1) > 0]
    us_data = us_data.groupby(['Province/State'])['Deaths'].sum().reset_index()
    us_data_death = us_data[us_data['Deaths'] > 0]
    state_fig = px.bar(us_data_death, x='Province/State', y='Deaths',
                       title='Province wise deaths reported of COVID-19 in China', text='Deaths')
    state_fig.show()


def show_death_cases_by_province_or_state(country, data):
    # state/province wise death cases of Novel Coronavirus (COVID-19) of any country
    return

def show_confirmed_cases_by_province_or_state(country, data):
    # state/province wise confirmed cases of Novel Coronavirus (COVID-19) of any country
    return

def show_active_cases_by_province_or_state(country, data):
    # state/province wise active cases of Novel Coronavirus (COVID-19) of any country
    return



def plot_total_deaths_confirmed_recovered_active_by_country_where_deaths_is_greater_than_150(data):
    #  plot (lines) of total deaths, confirmed, recovered and active cases Country wise where deaths greater than 150.
    covid_data = pd.read_csv(data, usecols=['Last Update', 'Country/Region', 'Confirmed', 'Deaths', 'Recovered'])
    covid_data['Active'] = covid_data['Confirmed'] - covid_data['Deaths'] - covid_data['Recovered']

    r_data = covid_data.groupby(["Country/Region"])["Deaths", "Confirmed", "Recovered", "Active"].sum().reset_index()
    r_data = r_data.sort_values(by='Deaths', ascending=False)
    r_data = r_data[r_data['Deaths'] > 150]
    plt.figure(figsize=(15, 5))
    plt.plot(r_data['Country/Region'], r_data['Deaths'], color='red')
    plt.plot(r_data['Country/Region'], r_data['Confirmed'], color='green')
    plt.plot(r_data['Country/Region'], r_data['Recovered'], color='blue')
    plt.plot(r_data['Country/Region'], r_data['Active'], color='black')

    plt.title('Total Deaths(>150), Confirmed, Recovered and Active Cases by Country')
    plt.show()

def plot_total_deaths_by_country_where_deaths_is_greater_than_150(data):
    return
    #  plot (lines) of total deaths cases Country wise where deaths greater than 150.

def plot_total_recovered_by_country_where_recovered_is_greater_than_10(data):
    #  plot (lines) of total deaths, confirmed, recovered and active cases Country wise where deaths greater than 150.

    return
