import pandas_helper as pandas_helper

data_url = "https://raw.githubusercontent.com/CSSEGISandData/COVID-19/master/csse_covid_19_data/csse_covid_19_daily_reports/03-19-2020.csv"
def main():
    # pandas_helper.show_world_wide_cases_overtime(
    #     data_url,
    #     "plotly_dark")
    # pandas_helper.show_active_cases(data_url)
    # pandas_helper.plot_total_deaths_confirmed_recovered_active_by_country_where_deaths_is_greater_than_150(data_url)
    # pandas_helper.plot_total_by_country_where_deaths_is_greater_than_150(data_url)
    pandas_helper.show_active_cases_by_province_or_state(country="China", data=data_url)
# Press the green button in the gutter to run the script.
if __name__ == '__main__':
    main()

# See PyCharm help at https://www.jetbrains.com/help/pycharm/
